#!/usr/bin/env bash

NOTIFY_ICON=/usr/share/icons/Papirus/32x32/apps/system-software-update.svg

get_total_updates() { UPDATES=$(checkupdates 2>/dev/null | wc -l); }

while true; do
    get_total_updates

    # notify user of updates
    if hash notify-send &>/dev/null; then
        if (( UPDATES > 20 )); then
            notify-send -u critical -i $NOTIFY_ICON \
                "Você precisa atualizar o sistema!!" "$UPDATES novos pacotes"
        elif (( UPDATES > 9 )); then
            notify-send -u normal -i $NOTIFY_ICON \
                "Você deve atualizar em breve" "$UPDATES novos pacotes"
        elif (( UPDATES > 2 )); then
            notify-send -u low -i $NOTIFY_ICON \
                "$UPDATES novos pacotes"
        fi
    fi

    # when there are updates available
    # every 10 seconds another check for updates is done
    while (( UPDATES > 0 )); do
        if (( UPDATES == 1 )); then
            echo "$UPDATES"
        elif (( UPDATES > 1 )); then
            echo "$UPDATES"
        else
            echo "OK"
        fi
        sleep 10
        get_total_updates
    done

    # when no updates are available, use a longer loop, this saves on CPU
    # and network uptime, only checking once every 30 min for new updates
    while (( UPDATES == 0 )); do
        echo "OK"
        sleep 1800
        get_total_updates
    done
done
