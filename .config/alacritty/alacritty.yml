# Configuration for Alacritty, the GPU enhanced terminal emulator.
env:
  TERM: xterm-256color
  LANG: "pt_BR.UTF-8"
  LC_CTYPE: pt_BR.UTF-8
window:
  dynamic_title: true
  padding:
    x: 4
    y: 4
  dynamic_padding: false

  title: Alacritty

  class:
    instance: Alacritty
    general: Alacritty

    gtk_theme_variant: None

scrolling:
  history: 10000
  multiplier: 1

font:
  normal:
    family: JetBrains Mono Nerd Font
    style: Medium
  bold:
    family: JetBrains Mono Nerd Font
    style: Bold
  italic:
    family: monospace
    style: Italic
  bold_italic:
    family: monospace
    style: Bold Italic
  size: 10

  offset:
    x: -1
    y: -1

  glyph_offset:
    x: 0
    y: 0

draw_bold_text_with_bright_colors: false

#colors:
#  primary:
#    background: '#191919'
#    foreground: '#d8dee9'
#
#  cursor:
#    text: '#191919'
#    cursor: '#d8dee9'
#
#  vi_mode_cursor:
#    text: CellBackground
#    cursor: CellForeground
#
#  selection:
#    text: '#191919'
#    background: '#d8dee9'
#
#  normal:
#    black:   '#191919'
#    red:     '#b02626'
#    green:   '#40a62f'
#    yellow:  '#f2e635'
#    blue:    '#314ad0'
#    magenta: '#b30ad0'
#    cyan:    '#32d0fc'
#    white:   '#acadb1'
#
#  bright:
#    black:   '#36393d'
#    red:     '#ce2727'
#    green:   '#47c930'
#    yellow:  '#fff138'
#    blue:    '#2e4bea'
#    magenta: '#cc15ed'
#    cyan:    '#54d9ff'
#    white:   '#dbdbdb'

#  dim:
#    black:   '#676f78'
#    red:     '#b55454'
#    green:   '#78a670'
#    yellow:  '#faf380'
#    blue:    '#707fd0'
#    magenta: '#c583d0'
#    cyan:    '#8adaf1'
#    white:   '#e0e3e7'

colors:
  primary:
    background: '#282a36'
    foreground: '#f8f8f2'
    bright_foreground: '#ffffff'
  cursor:
    text: CellBackground
    cursor: CellForeground
  vi_mode_cursor:
    text: CellBackground
    cursor: CellForeground
  search:
    matches:
      foreground: '#44475a'
      background: '#50fa7b'
    focused_match:
      foreground: '#44475a'
      background: '#ffb86c'
    bar:
      background: '#282a36'
      foreground: '#f8f8f2'
  hints:
    start:
      foreground: '#282a36'
      background: '#f1fa8c'
    end:
      foreground: '#f1fa8c'
      background: '#282a36'
  line_indicator:
    foreground: None
    background: None
  selection:
    text: CellForeground
    background: '#44475a'
  normal:
    black: '#21222c'
    red: '#ff5555'
    green: '#50fa7b'
    yellow: '#f1fa8c'
    blue: '#bd93f9'
    magenta: '#ff79c6'
    cyan: '#8be9fd'
    white: '#f8f8f2'
  bright:
    black: '#6272a4'
    red: '#ff6e6e'
    green: '#69ff94'
    yellow: '#ffffa5'
    blue: '#d6acff'
    magenta: '#ff92df'
    cyan: '#a4ffff'

bell:
  animation: EaseOutExpo
  duration: 0
  color: '#ffffff'

background_opacity: 1

selection:
  semantic_escape_chars: ",│`|:\"' ()[]{}<>\t"

  save_to_clipboard: false

cursor:
  style: Underline
  shape: Underline
  blinking: on
  blink-interval: 1
  unfocused_hollow: true
  vi_mode_style: Block

live_config_reload: true

shell:
  program: /usr/bin/zsh


working_directory: None

mouse:
  double_click: { threshold: 300 }
  triple_click: { threshold: 300 }

  #mouse_bindings:
  #  - { mouse: Middle, action: Copy }

key_bindings:
  # (Windows, Linux, and BSD only)
  - { key: P,        mods: Control,       action: Paste            }
  - { key: Insert,   mods: Shift,         action: Paste            }
  - { key: Slash,    mods: Control,       chars: "gc"              }
  - { key: Y,        mods: Control,       action: Copy             }
  - { key: Key0,     mods: Control,       action: ResetFontSize    }
  - { key: Equals,   mods: Control,       action: IncreaseFontSize }
  - { key: Plus,     mods: Control,       action: IncreaseFontSize }
  - { key: Minus,    mods: Control,       action: DecreaseFontSize }
  # Vi Mode
  - { key: Space,  mods: Control,       mode: Vi, action: ScrollToBottom          }
  - { key: Space,  mods: Control,                 action: ToggleViMode            }
  - { key: I,                           mode: Vi, action: ScrollToBottom          }
  - { key: I,                           mode: Vi, action: ToggleViMode            }
  - { key: C,      mods: Control,       mode: Vi, action: ScrollToBottom          }
  - { key: C,      mods: Control,       mode: Vi, action: ToggleViMode            }
  - { key: Escape,                      mode: Vi, action: ClearSelection          }
  - { key: Y,      mods: Control,       mode: Vi, action: ScrollLineUp            }
  - { key: E,      mods: Control,       mode: Vi, action: ScrollLineDown          }
  - { key: G,                           mode: Vi, action: ScrollToTop             }
  - { key: G,      mods: Shift,         mode: Vi, action: ScrollToBottom          }
  - { key: B,      mods: Control,       mode: Vi, action: ScrollPageUp            }
  - { key: F,      mods: Control,       mode: Vi, action: ScrollPageDown          }
  - { key: U,      mods: Control,       mode: Vi, action: ScrollHalfPageUp        }
  - { key: D,      mods: Control,       mode: Vi, action: ScrollHalfPageDown      }
  - { key: Y,                           mode: Vi, action: Copy                    }
  - { key: Y,                           mode: Vi, action: ClearSelection          }
  - { key: V,                           mode: Vi, action: ToggleNormalSelection   }
  - { key: V,      mods: Shift,         mode: Vi, action: ToggleLineSelection     }
  - { key: V,      mods: Control,       mode: Vi, action: ToggleBlockSelection    }
  - { key: V,      mods: Alt,           mode: Vi, action: ToggleSemanticSelection }
  - { key: Return,                      mode: Vi, action: Open                    }
  - { key: K,                           mode: Vi, action: Up                      }
  - { key: J,                           mode: Vi, action: Down                    }
  - { key: H,                           mode: Vi, action: Left                    }
  - { key: L,                           mode: Vi, action: Right                   }
  - { key: Up,                          mode: Vi, action: Up                      }
  - { key: Down,                        mode: Vi, action: Down                    }
  - { key: Left,                        mode: Vi, action: Left                    }
  - { key: Right,                       mode: Vi, action: Right                   }
  - { key: Key0,                        mode: Vi, action: First                   }
  - { key: Key4,            mode: Vi, action: Last                    }
  - { key: Key6,   mods: Shift,         mode: Vi, action: FirstOccupied           }
  - { key: H,      mods: Shift,         mode: Vi, action: High                    }
  - { key: M,      mods: Shift,         mode: Vi, action: Middle                  }
  - { key: L,      mods: Shift,         mode: Vi, action: Low                     }
  - { key: B,                           mode: Vi, action: SemanticLeft            }
  - { key: W,                           mode: Vi, action: SemanticRight           }
  - { key: E,                           mode: Vi, action: SemanticRightEnd        }
  - { key: B,      mods: Shift,         mode: Vi, action: WordLeft                }
  - { key: W,      mods: Shift,         mode: Vi, action: WordRight               }
  - { key: E,      mods: Shift,         mode: Vi, action: WordRightEnd            }
  - { key: Key5,   mods: Shift,         mode: Vi, action: Bracket                 }
  - { key: Slash,                       mode: Vi, action: SearchForward           }
  - { key: Slash,  mods: Shift,         mode: Vi, action: SearchBackward          }
  - { key: N,                           mode: Vi, action: SearchNext              }
  - { key: N,      mods: Shift,         mode: Vi, action: SearchPrevious          }

debug:
  render_timer: false

  persistent_logging: false

  log_level: Warn

  print_events: false
